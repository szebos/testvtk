#ifndef TESTQTCHART_H
#define TESTQTCHART_H

//Required for vtk 6
#include <vtkAutoInit.h>
 VTK_MODULE_INIT(vtkRenderingOpenGL);
#define vtkRenderingCore_AUTOINIT 4(vtkInteractionStyle,vtkRenderingFreeType,vtkRenderingFreeTypeOpenGL,vtkRenderingOpenGL)
#define vtkRenderingVolume_AUTOINIT 1(vtkRenderingVolumeOpenGL)

#include <QVTKWidget.h>
#include <vtkChartXY.h>
#include <vtkContextView.h>

class testQtChart : public QVTKWidget
{
    Q_OBJECT
public:
    explicit testQtChart(QWidget *parent = 0);

signals:

public slots:
private:
        vtkChartXY *myChart;
        vtkContextView *myViewCharts;
};

#endif // TESTQTCHART_H
