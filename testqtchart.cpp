#include "testqtchart.h"

#include "vtkTable.h"
#include "vtkFloatArray.h"
#include "vtkPlot.h"
#include "vtkPen.h"
#include "vtkTextProperty.h"
#include "vtkAxis.h"
#include "vtkRenderWindow.h"
#include "vtkRenderer.h"
#include "vtkContextScene.h"

#include "vtkXYPlotActor.h"
#include "vtkMath.h"
#include "vtkDoubleArray.h"
#include "vtkFieldData.h"

#define VTK_CREATE(type, name) \
    vtkSmartPointer<type> name = vtkSmartPointer<type>::New()

testQtChart::testQtChart(QWidget *parent) :
    QVTKWidget(parent)
{

    // Create a table with some points in it
    vtkSmartPointer<vtkTable> table =
      vtkSmartPointer<vtkTable>::New();

    vtkSmartPointer<vtkFloatArray> arrX =
      vtkSmartPointer<vtkFloatArray>::New();
    arrX->SetName("X Axis");
    table->AddColumn(arrX);

    vtkSmartPointer<vtkFloatArray> arrC =
      vtkSmartPointer<vtkFloatArray>::New();
    arrC->SetName("Cosine");
    table->AddColumn(arrC);

    vtkSmartPointer<vtkFloatArray> arrS =
      vtkSmartPointer<vtkFloatArray>::New();
    arrS->SetName("Sine");
    table->AddColumn(arrS);

    // Fill in the table with some example values
    int numPoints = 69;
    float inc = 7.5 / (numPoints-1);
    table->SetNumberOfRows(numPoints);
    for (int i = 0; i < numPoints; ++i)
    {
      table->SetValue(i, 0, i * inc);
      table->SetValue(i, 1, cos(i * inc));
      table->SetValue(i, 2, sin(i * inc));
    }

        VTK_CREATE(vtkChartXY,myChart);


        vtkPlot *line = myChart->AddPlot(vtkChart::LINE);
      #if VTK_MAJOR_VERSION <= 5
        line->SetInput(table, 0, 1);
      #else
        line->SetInputData(table, 0, 1);
      #endif
        line->SetColor(0, 255, 0, 255);
        line->SetWidth(1.0);
        line = myChart->AddPlot(vtkChart::LINE);
      #if VTK_MAJOR_VERSION <= 5
        line->SetInput(table, 0, 2);
      #else
        line->SetInputData(table, 0, 2);
      #endif
        line->SetColor(255, 0, 0, 255);
        line->SetWidth(5.0);
        line->GetPen()->SetLineType(2);//For dotted line, can be from 2 to 5 for different dot patterns

        vtkStdString title("An XY chart example");
        myChart->SetTitle(title);
        myChart->GetTitleProperties()->SetFontFamilyToArial();
        myChart->GetTitleProperties()->SetColor(1.,1.,1.);
        myChart->GetTitleProperties()->ShadowOn();
        myChart->GetTitleProperties()->SetShadowOffset(100,100);
        myChart->GetTitleProperties()->SetFontSize(24);
        myChart->GetTitleProperties()->SetVerticalJustificationToCentered();

        myChart->GetAxis(vtkAxis::LEFT)->GetTitleProperties()->SetFontFamilyToArial();
        myChart->GetAxis(vtkAxis::LEFT)->GetTitleProperties()->SetColor(1.,1.,1.);
//        myChart->GetAxis(vtkAxis::LEFT)->GetTitleProperties()->ShadowOn();
//        myChart->GetAxis(vtkAxis::LEFT)->GetTitleProperties()->SetShadowOffset(100,100);
//        myChart->GetAxis(vtkAxis::LEFT)->GetTitleProperties()->SetFontSize(18);
        myChart->GetAxis(vtkAxis::LEFT)->SetTitle(vtkStdString("Y"));

//        myChart->GetAxis(vtkAxis::BOTTOM)->GetTitleProperties()->SetFontFamilyToArial();
//        myChart->GetAxis(vtkAxis::BOTTOM)->GetTitleProperties()->SetColor(1.,1.,1.);
//        myChart->GetAxis(vtkAxis::BOTTOM)->GetTitleProperties()->ShadowOn();
//        myChart->GetAxis(vtkAxis::BOTTOM)->GetTitleProperties()->SetShadowOffset(100,100);
//        myChart->GetAxis(vtkAxis::BOTTOM)->GetTitleProperties()->SetFontSize(18);
        myChart->GetAxis(vtkAxis::BOTTOM)->SetTitle(vtkStdString("X"));


        VTK_CREATE(vtkContextView,myViewCharts);
        myViewCharts->GetScene()->AddItem(myChart);
        myViewCharts->SetInteractor(GetInteractor());
        SetRenderWindow(myViewCharts->GetRenderWindow());
        myViewCharts->GetRenderer()->SetBackground(81/255.,113/255.,150/255.);
        myViewCharts->GetRenderWindow()->SetMultiSamples(4);
/***********************************
 * Legacy plot
 * *********************************/
//        /*
        VTK_CREATE(vtkXYPlotActor,plotActor);
        plotActor->ExchangeAxesOff();
        plotActor->SetLabelFormat( "%g" );
        plotActor->SetXTitle( "Level" );
        plotActor->SetYTitle( "Frequency" );
        plotActor->SetXValuesToValue();
        for (unsigned int i = 0 ; i < 2 ; i++)
          {
          vtkSmartPointer<vtkDoubleArray> array_s =
              vtkSmartPointer<vtkDoubleArray>::New();
          vtkSmartPointer<vtkFieldData> field =
              vtkSmartPointer<vtkFieldData>::New();
          vtkSmartPointer<vtkDataObject> data =
              vtkSmartPointer<vtkDataObject>::New();

//          for (int b = 0; b < 30; b++)   /// Assuming an array of 30 elements
//            {
//            double val = vtkMath::Random(0.0,2.0);
//            array_s->InsertValue(b,val);
//            }
          for (int i = 0; i < numPoints; ++i)
          {
//            table->SetValue(i, 0, i * inc);
//            table->SetValue(i, 1, cos(i * inc));
//            table->SetValue(i, 2, sin(i * inc));
              array_s->InsertValue(i,cos(i * inc));
          }
          field->AddArray(array_s);
          data->SetFieldData(field);
          plotActor->AddDataObjectInput(data);
        }
        myViewCharts->GetRenderer()->AddActor2D(plotActor);
//        */

}
