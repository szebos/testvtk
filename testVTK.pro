QT       += core gui opengl

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TEMPLATE = app


SOURCES += main.cpp \
    testqtchart.cpp

HEADERS += \
    testqtchart.h

INCLUDEPATH += "../testVTK"

include(../turbosimfw/VTKimports.pri)
